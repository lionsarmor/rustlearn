pub fn run() {
    //print to console
    println!("hello from the end of the world");

    println!("Number: {}", 1);

    // basic formatting
    println!("{} is from {}", "james", "mass");

    // positional atguments
    println!(
        "{1} is from mass and {0} likes to {2}",
        "brad", "Mass", "CODE"
    );

    //Named arguments
    println!(
        "{name} likes to play {activity}",
        name = "john",
        activity = "baseball"
    );

    // placeholder traits
    println!("Binary: {:b} Hex: {:x} Octal: {:o}", 10, 10, 10);

    // placeholder for debug trait
    println!("{:?}", (12, true, "hello"));

    // basic math
    println!("10 + 10 = {}", 10 + 10);
}
