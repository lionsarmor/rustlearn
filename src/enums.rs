// Enums - Types that have a few deffinate values

enum Movement {
    //varients
    Up,
    Down,
    Left,
    Right,
}

fn move_avatar(m: Movement) {
    // Perform action depending on info
    match m {
        Movement::Up => println!("Avatar Moving UP"),
        Movement::Down => println!("Avatar Moving DOWN"),
        Movement::Left => println!("Avatar Moving LEFT"),
        Movement::Right => println!("Avatar Moving RIGHT"),
    }
}

pub fn run() {
    let avatar1 = Movement::Left;
    let avatar2 = Movement::Up;
    let avatar3 = Movement::Right;
    let avatar4 = Movement::Down;

    move_avatar(avatar1);
    move_avatar(avatar2);
    move_avatar(avatar3);
    move_avatar(avatar4);
}
