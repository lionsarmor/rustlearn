// Vriables hold primitive data or references to data
// Variables are imutable by default
// rust is a block=scoped language

pub fn run() {
    let name = "james";
    let mut age = 34;
    println!("My name is {} and i am {}", name, age);

    age = 26;
    println!("My name is {} and i am {}", name, age);

    // Define Constant
    const ID: i32 = 001;
    println!("ID: {}", ID);

    //Assign multible variables at once
    let (my_name, my_age) = ("James", 34);
    println!("{} is {}", my_name, my_age);
}
