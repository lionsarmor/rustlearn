// Primitive str - Immutable fixed-lenght string somewhere in memory
// String = Growable, heap allocated data structure - use when you ned to modify your own string data

pub fn run() {
    //let hello = "hello";

    let mut hello = String::from("Hello ");

    // Get Length
    println!("Length: {}", hello.len());

    // push char
    hello.push('W');

    // push string
    hello.push_str("orld!");

    // Capacity in bytes
    println!("Capacity: {}", hello.capacity());

    // check if empty
    println!("Empty: {}", hello.is_empty());

    // Contains some sub string
    println!("Contains 'World' {}", hello.contains("World"));

    // Replace
    println!("Replace: {}", hello.replace("World", "There"));

    // Loop through string by white space
    for word in hello.split_whitespace() {
        println!("{}", word);
    }

    // Create string with capacity
    let mut s = String::with_capacity(10);
    s.push('a');
    s.push('b');

    println!("{}", s);

    // Assertion Test
    assert_eq!(2, s.len());
    assert_eq!(11, s.capacity());
}
